# Serbian translation for gtk-vnc.
# Copyright (C) 2012 gtk-vnc's COPYRIGHT HOLDER
# This file is distributed under the same license as the gtk-vnc package.
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: gtk-vnc master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-31 15:11+0100\n"
"PO-Revision-Date: 2012-10-17 11:16+0200\n"
"Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <gnom@prevod.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Project-Style: gnome\n"

#: src/vncdisplay.c:175
msgid "Enables debug output"
msgstr "Укључује излаз за уклањање грешака"

#: src/vncdisplay.c:3502
msgid "GTK-VNC Options:"
msgstr "ГТК-ВНЦ опције:"

#: src/vncdisplay.c:3502
msgid "Show GTK-VNC Options"
msgstr "Приказује ГТК-ВНЦ опције"
